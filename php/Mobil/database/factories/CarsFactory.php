<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Cars;
use Faker\Generator as Faker;

$factory->define(Cars::class, function (Faker $faker) {

    return [

        'make' => $faker->company,

        'model' => $faker->word,

        'year' => $faker->randomNumber,

    ];

});
